import { Injectable } from '@angular/core';

import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Employe } from './employe'


@Injectable()

export class EmployeService {

  constructor(private http: Http) {

  }

  private headers = new Headers({ 'Content-Type': 'application/json' });
  private employesUrl = '/api/employes';

  getEmployes(): Promise<Employe[]> {
    return this.http.get(this.employesUrl + "/allEmployes")
      .toPromise()
      .then(response => response.json() as Employe[])
      .catch(this.handleError);
  }


  getEmploye(id: number): Promise<Employe> {
    const url = `${this.employesUrl}/getEmploye/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as Employe)
      .catch(this.handleError);
  }


  createEmploye(employe: Employe): Promise<Employe> {
    return this.http
      .post(this.employesUrl + "/createEmploye", JSON.stringify(employe), { headers: this.headers })
      .toPromise()
      .then(res => res.json() as Employe)
      .catch(this.handleError);
  }

  updateEmploye(employe: Employe): Promise<Employe> {
    return this.http
      .post(this.employesUrl + "/updateEmploye", JSON.stringify(employe), { headers: this.headers })
      .toPromise()
      .then(() => employe)
      .catch(this.handleError);
  }

  deleteEmploye(employe: Employe): Promise<void> {
    const url = `${this.employesUrl}/deleteEmploye/${employe.id}`;
    return this.http.get(url, { headers: this.headers })
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
