import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { EmployeInfoComponent } from '../employe-info/employe-info.component';
import { EmployesComponent } from '../employes/employes.component';

const routes: Routes = [
  { path: 'employe-info/:id', component: EmployeInfoComponent },
  { path: 'employes', component: EmployesComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
