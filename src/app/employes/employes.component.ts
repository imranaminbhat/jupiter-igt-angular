import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Employe } from '../employe';
import { EmployeService } from '../employe.service';

@Component({
  selector: 'app-employes',
  templateUrl: './employes.component.html',
  styleUrls: ['./employes.component.css']
})
export class EmployesComponent implements OnInit {

  employes: Employe[];
  selectedEmploye: Employe;
  newEmploye: Employe;

  constructor(private router: Router, private employeService: EmployeService) {

  }

  ngOnInit() {
    this.employeService.getEmployes().then(employes => this.employes = employes);
    this.newEmploye = new Employe();
  }

  createEmploye(employe: Employe): void {

    this.employeService.createEmploye(employe)
      .then(employe => {
        this.employes.push(employe);
        this.selectedEmploye = null;
      });
  }

  deleteEmploye(employe: Employe): void {
    this.employeService
      .deleteEmploye(employe)
      .then(() => {
        this.employes = this.employes.filter(h => h !== employe);
        if (this.selectedEmploye === employe) { this.selectedEmploye = null; }
      });
  }

  showInfo(employe: Employe): void {
    this.selectedEmploye = employe;
    this.router.navigate(['/employe-info', this.selectedEmploye.id]);
  }
}
