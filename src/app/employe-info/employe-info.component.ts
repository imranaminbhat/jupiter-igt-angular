import 'rxjs/add/operator/switchMap'
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';

import { Employe } from '../employe';
import { EmployeService } from '../employe.service'


@Component({
  selector: 'app-employe-info',
  templateUrl: './employe-info.component.html',
  styleUrls: ['./employe-info.component.css']
})
export class EmployeInfoComponent implements OnInit {

  employe: Employe;

  constructor(
    private employeService: EmployeService,
    private route: ActivatedRoute,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.route.params.switchMap((params: Params) => this.employeService.getEmploye(params['id']))
      .subscribe(employe => this.employe = employe);
  }
  updateEmploye(): void {
    this.employeService.updateEmploye(this.employe);
    this.goBack();
  }
  goBack(): void {
    this.location.back();
  }

}
