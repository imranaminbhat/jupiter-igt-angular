import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { EmployeInfoComponent } from './employe-info/employe-info.component';
import { EmployesComponent } from './employes/employes.component';
import { EmployeService } from './employe.service';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing/app-routing.module';
import { MaterialModule, MdList, MdListItem, MdTableModule, MdBasicChip } from '@angular/material';


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterialModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  declarations: [
    AppComponent,
    EmployesComponent,
    EmployeInfoComponent
  ],
  bootstrap: [AppComponent],
  providers: [EmployeService],
})
export class AppModule { }
