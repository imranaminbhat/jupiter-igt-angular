import { EmployesPage } from './app.po';

describe('employes App', () => {
  let page: EmployesPage;

  beforeEach(() => {
    page = new EmployesPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
